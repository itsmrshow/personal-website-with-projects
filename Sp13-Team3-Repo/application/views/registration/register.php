
<?php echo validation_errors(); ?>

<?php echo form_open('registration/index', $attributes) ?>

	<div class="clearfix">
		<fieldset class="pull-left span6">
			<div class="control-group">
				<label class="control-label" for="date">Date:</label>
				<div class="controls">
					<input type="text" class="span2" id="date" name="date" value="<?php $yo = set_value('date') == "" ? Date("m-d-Y") : set_value('date'); echo $yo; ?>" />
				</div>
			</div>
		</fieldset>
		<fieldset class="pull-right span6">
			<div class="control-group">
				<label class="control-label" for="advisor">Adviser:</label>
				<div class="controls">
					<select id="advisor" name="advisor">
						<option value="Axtell, Robert">Axtell, Robert</option>
						<option value="Calahan, Susan">Calahan, Susan</option>
						<option value="Davis, Charles">Davis, Charles</option>
						<option value="Fede, Marybeth">Fede, Marybeth</option>
						<option value="Gregory, Robert">Gregory, Robert</option>
						<option value="Hannah, Corey">Hannah, Corey</option>
						<option value="Kemler, David">Kemler, David</option>
						<option value="Lamonica, Aukje">Lamonica, Aukje</option>
						<option value="Latchman, Peter">Latchman, Peter</option>
						<option value="Lunn, William">Lunn, William</option>
						<option value="Marino, Doris">Marino, Dorris</option>
						<option value="Misasi, Sharon">Misasi, Sharon</option>
						<option value="Morin, Gary">Morin, Gary</option>
						<option value="Panichas, Pat">Panichas, Pat</option>
						<option value="Rauschenbach, Jim">Rauschenbach, Jim</option>
						<option value="Rothbard, Matt">Rothbard, Matt</option>
						<option value="Swartz, Daniel">Swartz, Daniel</option>
						<option value="Yang, Jinjin">Yang, Jinjin</option>
					</select>
				</div>
			</div>
		</fieldset>
	</div>
	<h1 class="text-center">SCSU EXS Registration</h1>
	<div class="clearfix">
		<fieldset class="pull-left span6">
			<div class="control-group">
				<label class="control-label" for="student">Student:</label>
				<div class="controls">
					<input type="text" id="student" name="student" value="<?php echo set_value('student'); ?>" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Circle EXS Program:</label>
				<div class="controls">
					<input type="radio" class="radio" name="exs" value="at" checked="checked" /> AT
					<input type="radio" class="radio" name="exs" value="hp" /> HP
					<input type="radio" class="radio" name="exs" value="te" /> TE
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Accepted into Program:</label>
				<div class="controls">
					<input type="radio" class="radio" name="accept" value="yes" checked="checked" /> Yes
					<input type="radio" class="radio" name="accept" value="no" /> No
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="gpa">Current GPA:</label>
				<div class="controls">
					<input type="text" class="span1" id="gpa" name="gpa" value="<?php echo set_value('gpa'); ?>" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="semester">Semester Apply to Department:</label>
				<div class="controls-row">
					<select id="season" name="season" class="span2">
						<option value="Spring">Spring</option>
						<option value="Fall">Fall</option>
					</select>
					<input type="text" id="semester" name="semester" class="span1" maxlength="4" value="<?php echo set_value('semester'); ?>" />
				</div>
			</div>
		</fieldset>
		<fieldset class="pull-right span6">
			<div class="control-group">
				<label class="control-label" for="id">ID #:</label>
				<div class="controls">
					<input type="text" class="span2" id="id" name="id" maxlength="8" value="<?php echo set_value('id'); ?>" />
				</div>
			</div>
            	<div class="control-group">
				<label class="control-label" for="pin">Get Pin: </label>
				<div class="controls" style="padding-top:5px">
					<a href="http://myscsu.southernct.edu" target="blah"> Click Here </a>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="pin">PIN #:</label>
				<div class="controls">
					<input type="text" class="span2" id="pin" name="pin" maxlength="6" value="<?php echo set_value('pin'); ?>" />
				</div>
			</div>
		</fieldset>
	</div>
	<h3>
		<select id="csseason" name="csseason" class="span2">
			<option value="Spring">Spring</option>
			<option value="Fall">Fall</option>
		</select>
		<input type="text" class="span1" name="csyear" id="csyear" maxlength="4" />
		Course Selections:
	</h3>
	<div class="clearfix">
		<fieldset class="pull-left span6">
			<label class="control-label">&nbsp;</label>
			<div class="control-group">
				<div class="controls" style="margin-left:auto;">
					<table>
						<thead>
							<tr>
								<th>EXS</th>
								<th>Course</th>
								<th>Credit</th>
								<th>CRN #</th>
								<th>Days</th>
								<th>Times</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<select class="span2" data-id="1" name="exs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="1" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="2" name="exs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="2" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="3" name="exs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="3" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="4" name="exs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="4" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="5" name="exs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="5" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="6" name="exs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="6" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="7" name="exs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="7" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="8" name="exs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="8" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</fieldset>
	</div>
	<h4>Based on the courses selected above, the student should take the following courses:</h4>
	<div class="clearfix" style="width:750px;">
		<fieldset class="pull-left span4">
			<div class="control-group">
				<lable class="control-label">&nbsp;</lable>
				<div class="controls" style="margin-left: auto;">
				  <table>
					  <thead>
							<tr>
								<th>EXS</th>
								<th>
									<select name="pre1season" class="span2">
										<option value="Spring">Spring</option>
										<option value="Fall">Fall</option>
									</select>
									<input type="text" maxlength="4" name="pre1year" class="span1" />
								</th>
								<th>Cr.</th>
							</tr>
						</thead>
						<tbod>
							<tr>
								<td>
									<select class="span2" data-id="cn1" name="cnexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn1" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cn2" name="cnexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn2" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cn3" name="cnexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn3" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cn4" name="cnexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn4" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cn5" name="cnexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn5" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cn6" name="cnexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn6" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cn7" name="cnexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn7" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cn8" name="cnexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn8" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
						</tbod>
					</table>
				</div>
			</div>
		</fieldset>
		<fieldset class="pull-right span4">
			<div class="control-group">
				<label class="control-label">&nbsp;</label>
				<div class="controls" style="margin-left: auto;">
				  <table>
					  <thead>
							<tr>
								<th>EXS</th>
								<th>
									<select name="pre2season" class="span2">
										<option value="Spring">Spring</option>
										<option value="Fall">Fall</option>
									</select>
									<input type="text" maxlength="4" name="pre2year" class="span1" />
								</th>
								<th>Cr.</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<select class="span2" data-id="cnf1" name="cnfexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf1" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cnf2" name="cnfexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf2" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cnf3" name="cnfexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf3" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cnf4" name="cnfexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf4" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cnf5" name="cnfexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf5" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cnf6" name="cnfexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf6" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cnf7" name="cnfexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf7" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cnf8" name="cnfexs[]">
										<?php echo $exsOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf8" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</fieldset>
	</div>
	<div class="clearfix">
		<fieldset class="pull-left span6" style="width:1500px;">
			<div class="control-group">
				<label class="control-label">Student Notes:</label>
				<div class="controls" style="margin-left:40px;">
					<textarea name="studentNotes" id="studentNotes" class="span7" cols="80" rows="5"></textarea>
				</div>
			</div>
		</fieldset>
	</div>
	<div class="clearfix" id="chairNotes">
		<fieldset class="pull-left span6" style="width:1500px;">
			<div class="control-group">
				<label class="control-label">Chair Notes:</label>
				<div class="controls" style="margin-left:40px;">
					<textarea name="chairNotes" id="chairNotes" class="span7" cols="80" rows="5"></textarea>
				</div>
			</div>
		</fieldset>
	</div>
	<div class="clearfix">
		<fieldset class="pull-left span6">
			<div class="control-group">
				<label class="control-label">&nbsp;</label>
				<div class="controls">
					<button type="submit" class="btn btn-primary">Save changes</button>
                    <button type="button" onClick="window.print()" value="Print This Page" class="btn" />Print Page</button>
		    	</div>
			</div>
		</fieldset>
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function() {
		$('#date').datepicker({
			format: 'mm-dd-yyyy'
		});
		
		$('#season').change(function() {
			$('#ssn').html($(this).val());
		});
		
		$("select[name^=exs]").change(disableMe);
		$("select[name^=cnfexs]").change(disableMe);
		$("select[name^=cnexs]").change(disableMe);
	});
	
	var disableMe = function() {
		if($(this).val() != "0") {
			$('#'+$(this).attr('data-id')).val('');
			$('#'+$(this).attr('data-id')).attr('readonly', 'readonly');
			$('#'+$(this).attr('data-id')).css({'background-color':'#cccccc'});
		} else {
			$('#'+$(this).attr('data-id')).removeAttr('readonly');
			$('#'+$(this).attr('data-id')).css({'background-color':'#ffffff'});
		}
	};
</script>
