<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $title; ?></title>
		<meta charset="UTF-8" />
		
		<script type="text/javascript" src="<?php echo base_url('bootstrap/js/jquery.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap-datepicker.js')?>"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/bootstrap.css'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/main.css'); ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/datepicker.css'); ?>" />
	</head>
	<body>