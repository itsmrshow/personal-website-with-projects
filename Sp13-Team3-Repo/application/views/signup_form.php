<?php echo validation_errors(); ?>
<h1 align="center"> Create an Advisor Account!</h1>
<?php echo form_open('login/create_member', $attributes);?>
<div align="center">
  <h2>Personal Information
  </h2>
</div>
    <table width="200" border="0" align="center">
      <tr>
        <td><div align="center">First Name:
        </div></td>
        <td><?php echo form_input('first_name', ''); ?>
        </td>
      </tr>
      <tr>
        <td><div align="center">Last Name:</div></td>
        <td><?php echo form_input('last_name', ''); ?></td>
      </tr>
      <tr>
        <td><div align="center">EmailAddress:</div></td>
        <td><?php echo form_input('email_address', ''); ?>
      </td>
      </tr>
  </table>
	<div align="center">
      <h2>
         Login Info
        <?php $options = array('no' => 'No',
                  		   'yes' => 'Yes'); ?>
      </h2>
    </div>
  <table width="200" border="0" align="center">
    <tr>
      <td><div align="center">Username: </div></td>
      <td><?php echo form_input('username', set_value('username', '')); ?>
      </td>
    </tr>
    <tr>
      <td><div align="center">Password:</div></td>
      <td><?php echo form_password('password', set_value('password', '')); ?></td>
    </tr>
    <tr>
      <td><div align="center">Confirm Password:</div></td>
      <td><?php echo form_password('password2', set_value('password2', '')); ?>
      </td>
    </tr>
    <tr>
      <td><div align="center">Is Admin?</div></td>
      <td><?php
		echo form_dropdown('admin', set_value('admin', $options)); ?>
      </td>
    </tr>
    <tr>
    <td>
    <p align="center"><?php echo form_submit('submit', 'Create Account');?></p>
    </td>
    </tr>
  </table>
<?php echo validation_errors('<p class="error">'); ?>
	
