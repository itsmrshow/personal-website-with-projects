<h1 align="center"> Create an Advisor Account!</h1>
<style>
.error{
	color:#30F	
}
</style>
<?php echo form_open('change', $attributes);?>
<div align="center">
  <table width="200" border="0" align="center">
    <tr>
      <td><div align="center">Username: </div></td>
      <td><?php echo form_input('username', set_value('username', '')); ?>
      </td>
    </tr>
    <tr>
      <td><div align="center">Old Password:</div></td>
      <td><?php echo form_password('old_password', set_value('old_password', '')); ?></td>
    </tr>
    <tr>
      <td><div align="center">Password:</div></td>
      <td><?php echo form_password('new_password', set_value('new_password', '')); ?></td>
    </tr>
    <tr>
      <td><div align="center">Confirm Password:</div></td>
      <td><?php echo form_password('new_con_password', set_value('password', '')); ?>
      </td>
    </tr>
    <tr>
    <td>
    <p align="center"><?php echo form_submit('submit', 'Change Password');?></p>
    </td>
    </tr>
  </table>
  <?php echo validation_errors('<p class="error">'); ?>