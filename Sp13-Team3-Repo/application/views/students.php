<?php echo form_open('login/logout', $attributes);?>
<div align="center">
<table border="1">
<thead>
<th>Student ID</th>
<th>Registration Date</th>
<th>Advisor</th>
<th>Student</th>
<th>gpa</th>
<th>Current Semester</th>
<th>Edit Student</th>
</thead>

<tbody>
<?php foreach($students as $student): ?>
<tr>
<td>
<?php echo $student->student_id; ?>
</td>
<td>
<?php echo $student->reg_date; ?>
</td>
<td>
<?php echo $student->advisor; ?>
</td>
<td>
<?php echo $student->student; ?>
</td>
<td>
<?php echo $student->gpa; ?>
</td>
<td>
<?php echo $student->semester; ?>
</td>
<td>
<b><a href="../edit/id/<?php echo $student->student_id; ?>" target="content">Edit Student</a> | <a href="../edit/delete/<?php echo $student->student_id; ?>" target="content">Delete</a></b>
</td>
</tr>
<?php endforeach; ?>
</tbody>
</table>
</div>
<body>
</body>
</html>