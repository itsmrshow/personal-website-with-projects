
<?php echo validation_errors(); ?>

<?php echo form_open('edit/id/'.$studentId, $attributes) ?>

	<div class="clearfix">
		<fieldset class="pull-left span6">
			<div class="control-group">
				<label class="control-label" for="date">Date:</label>
				<div class="controls">
					<input type="text" class="span2" id="date" name="date" value="<?php $yo = set_value('date') == "" ? Date("m-d-Y") : set_value('date'); echo $yo; ?>" />
				</div>
			</div>
		</fieldset>
		<fieldset class="pull-right span6">
			<div class="control-group">
				<label class="control-label" for="advisor">Adviser:</label>
				<div class="controls">
					<select id="advisor" name="advisor">
						<option value="Axtell, Robert" <?php echo $a1; ?>>Axtell, Robert</option>
						<option value="Calahan, Susan" <?php echo $a2; ?>>Calahan, Susan</option>
						<option value="Davis, Charles" <?php echo $a3; ?>>Davis, Charles</option>
						<option value="Fede, Marybeth" <?php echo $a4; ?>>Fede, Marybeth</option>
						<option value="Gregory, Robert" <?php echo $a5; ?>>Gregory, Robert</option>
						<option value="Hannah, Corey" <?php echo $a6; ?>>Hannah, Corey</option>
						<option value="Kemler, David" <?php echo $a7; ?>>Kemler, David</option>
						<option value="Lamonica, Aukje" <?php echo $a8; ?>>Lamonica, Aukje</option>
						<option value="Latchman, Peter" <?php echo $a9; ?>>Latchman, Peter</option>
						<option value="Lunn, William" <?php echo $a10; ?>>Lunn, William</option>
						<option value="Marino, Doris" <?php echo $a11; ?>>Marino, Dorris</option>
						<option value="Misasi, Sharon" <?php echo $a12; ?>>Misasi, Sharon</option>
						<option value="Morin, Gary" <?php echo $a13; ?>>Morin, Gary</option>
						<option value="Panichas, Pat" <?php echo $a14; ?>>Panichas, Pat</option>
						<option value="Rauschenbach, Jim" <?php echo $a15; ?>>Rauschenbach, Jim</option>
						<option value="Rothbard, Matt" <?php echo $a16; ?>>Rothbard, Matt</option>
						<option value="Swartz, Daniel" <?php echo $a17; ?>>Swartz, Daniel</option>
						<option value="Yang, Jinjin" <?php echo $a18; ?>>Yang, Jinjin</option>
					</select>
				</div>
			</div>
		</fieldset>
	</div>
	<h1 class="text-center">SCSU EXS Registration</h1>
	<div class="clearfix">
		<fieldset class="pull-left span6">
			<div class="control-group">
				<label class="control-label" for="student">Student:</label>
				<div class="controls">
					<input type="text" id="student" name="student" value="<?php echo $student; ?>" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Circle EXS Program:</label>
				<div class="controls">
					<input type="radio" class="radio" name="exs" value="at" checked="checked" /> AT
					<input type="radio" class="radio" name="exs" value="hp" /> HP
					<input type="radio" class="radio" name="exs" value="te" /> TE
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Accepted into Program:</label>
				<div class="controls">
					<input type="radio" class="radio" name="accept" value="yes" <?php echo $yes; ?> /> Yes
					<input type="radio" class="radio" name="accept" value="no" <?php echo $no; ?> /> No
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="gpa">Current GPA:</label>
				<div class="controls">
					<input type="text" class="span1" id="gpa" name="gpa" value="" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="semester">Semester Apply to Department:</label>
				<div class="controls-row">
					<select id="season" name="season" class="span2">
						<option value="Spring" <?php echo $applySpring; ?>>Spring</option>
						<option value="Fall" <?php echo $applyFall; ?>>Fall</option>
					</select>
					<input type="text" id="semester" name="semester" class="span1" maxlength="4" value="<?php echo $applyYear; ?>" />
				</div>
			</div>
		</fieldset>
		<fieldset class="pull-right span6">
			<div class="control-group">
				<label class="control-label" for="id">ID #:</label>
				<div class="controls">
					<input readonly="readonly" type="text" class="span2" id="id" name="id" maxlength="8" value="<?php echo $studentId; ?>" />
				</div>
			</div>
            <div class="control-group">
				<label class="control-label" for="pin">Get Pin:</label>
				<div class="controls" style="padding-top:5px">
					<a href="http://myscsu.southernct.edu" target="blah"> Click Here </a>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="pin">PIN #:</label>
				<div class="controls">
					<input type="text" class="span2" id="pin" name="pin" maxlength="6" value="" />
				</div>
			</div>
		</fieldset>
	</div>
	<h3>
		<select id="csseason" name="csseason" class="span2">
			<option value="Spring" <?php echo $mainFall; ?>>Spring</option>
			<option value="Fall" <?php echo $mainSpring; ?>>Fall</option>
		</select>
		<input type="text" class="span1" name="csyear" id="csyear" maxlength="4" value="<?php echo $mainYear; ?>" />
		Course Selections:
	</h3>
	<div class="clearfix">
		<fieldset class="pull-left span6">
			<label class="control-label">&nbsp;</label>
			<div class="control-group">
				<div class="controls" style="margin-left:80px;">
					<table>
						<thead>
							<tr>
								<th>EXS</th>
								<th>Course</th>
								<th>Credit</th>
								<th>CRN #</th>
								<th>Days</th>
								<th>Times</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<select class="span2" data-id="1" name="exs[]">
										<?php echo $exsOptions1; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="1" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="2" name="exs[]">
										<?php echo $exsOptions2; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="2" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="3" name="exs[]">
										<?php echo $exsOptions3; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="3" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="4" name="exs[]">
										<?php echo $exsOptions4; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="4" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="5" name="exs[]">
										<?php echo $exsOptions5; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="5" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="6" name="exs[]">
										<?php echo $exsOptions6; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="6" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="7" name="exs[]">
										<?php echo $exsOptions7; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="7" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="8" name="exs[]">
										<?php echo $exsOptions8; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" id="8" name="course[]" />
								</td>
								<td>
									<select class="span1" name="credit[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span2" name="crn[]" />
								</td>
								<td>
									<input type="text" class="span2" name="days[]" />
								</td>
								<td>
									<input type="text" class="span3" name="times[]" />
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</fieldset>
	</div>
	<h4>Based on the courses selected above, the student should take the following courses:</h4>
	<div class="clearfix" style="width:750px;">
		<fieldset class="pull-left span4">
			<div class="control-group">
				<lable class="control-label">&nbsp;</lable>
				<div class="controls" style="margin-left: 40px;">
					<table>
						<thead>
							<tr>
								<th>EXS</th>
								<th>
									<select name="pre1season" class="span2">
										<option value="Spring" <?php echo $preSpring1; ?>>Spring</option>
										<option value="Fall" <?php echo $preFall1; ?>>Fall</option>
									</select>
									<input type="text" maxlength="4" name="pre1year" class="span1" value="<?php echo $preYear1; ?>" />
								</th>
								<th>Cr.</th>
							</tr>
						</thead>
						<tbod>
							<tr>
								<td>
									<select class="span2" data-id="cn1" name="cnexs[]">
										<?php echo $cexsOptions1; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn1" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cn2" name="cnexs[]">
										<?php echo $cexsOptions2; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn2" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cn3" name="cnexs[]">
										<?php echo $cexsOptions3; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn3" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cn4" name="cnexs[]">
										<?php echo $cexsOptions4; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn4" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cn5" name="cnexs[]">
										<?php echo $cexsOptions5; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn5" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cn6" name="cnexs[]">
										<?php echo $cexsOptions6; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn6" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cn7" name="cnexs[]">
										<?php echo $cexsOptions7; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn7" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cn8" name="cnexs[]">
										<?php echo $cexsOptions8; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cn8" name="cn2014[]" />
								</td>
								<td>
									<select class="span1" name="cr2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
						</tbod>
					</table>
				</div>
			</div>
		</fieldset>
		<fieldset class="pull-right span4">
			<div class="control-group">
				<label class="control-label">&nbsp;</label>
				<div class="controls" style="margin-left: 80px;">
					<table>
						<thead>
							<tr>
								<th>EXS</th>
								<th>
									<select name="pre2season" class="span2">
										<option value="Spring" <?php echo $preSpring2; ?>>Spring</option>
										<option value="Fall" <?php echo $preFall2; ?>>Fall</option>
									</select>
									<input type="text" maxlength="4" name="pre2year" class="span1" value="<?php echo $preYear2; ?>" />
								</th>
								<th>Cr.</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<select class="span2" data-id="cnf1" name="cnfexs[]">
										<?php echo $dexsOptions1; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf1" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cnf2" name="cnfexs[]">
										<?php echo $dexsOptions2; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf2" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cnf3" name="cnfexs[]">
										<?php echo $dexsOptions3; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf3" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cnf4" name="cnfexs[]">
										<?php echo $dexsOptions4; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf4" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cnf5" name="cnfexs[]">
										<?php echo $dexsOptions5; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf5" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cnf6" name="cnfexs[]">
										<?php echo $dexsOptions6; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf6" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cnf7" name="cnfexs[]">
										<?php echo $dexsOptions7; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf7" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<select class="span2" data-id="cnf8" name="cnfexs[]">
										<?php echo $dexsOptions8; ?>
									</select>
								</td>
								<td>
									<input type="text" class="span3" id="cnf8" name="cnf2014[]" />
								</td>
								<td>
									<select class="span1" name="crf2014[]">
										<?php echo $creditOptions; ?>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</fieldset>
	</div>
	<div class="clearfix">
		<fieldset class="pull-left span6" style="width:1500px;">
			<div class="control-group">
				<label class="control-label">Student Notes:</label>
				<div class="controls" style="margin-left:40px;">
					<textarea name="studentNotes" id="studentNotes" class="span7" cols="80" rows="5"></textarea>
				</div>
			</div>
		</fieldset>
	</div>
	<div class="clearfix" id="chairNotes">
		<fieldset class="pull-left span6" style="width:1500px;">
			<div class="control-group">
				<label class="control-label">Chair Notes:</label>
				<div class="controls" style="margin-left:40px;">
					<textarea name="chairNotes" id="chairNotes" class="span7" cols="80" rows="5"></textarea>
				</div>
			</div>
		</fieldset>
	</div>
	<div class="clearfix">
		<fieldset class="pull-left span6">
			<div class="control-group">
				<label class="control-label">&nbsp;</label>
				<div class="controls">
					<input type="hidden" name="studentId" value="<?php echo $studentId; ?>" />
					<button type="submit" class="btn btn-primary">Save changes</button>
                    <button type="button" onClick="window.print()" value="Print This Page" class="btn" />Print Page</button>

		    	</div>
			</div>
		</fieldset>
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function() {
		$('#date').datepicker({
			format: 'mm-dd-yyyy'
		});
		
		$('#season').change(function() {
			$('#ssn').html($(this).val());
		});
		
		$("select[name^=exs]").change(disableMe);
		$("select[name^=cnfexs]").change(disableMe);
		$("select[name^=cnexs]").change(disableMe);
	});
	
	var disableMe = function() {
		if($(this).val() != "0") {
			$('#'+$(this).attr('data-id')).val('');
			$('#'+$(this).attr('data-id')).attr('readonly', 'readonly');
			$('#'+$(this).attr('data-id')).css({'background-color':'#cccccc'});
		} else {
			$('#'+$(this).attr('data-id')).removeAttr('readonly');
			$('#'+$(this).attr('data-id')).css({'background-color':'#ffffff'});
		}
	};
</script>
