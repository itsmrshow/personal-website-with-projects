<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class change extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}
	public function index() {
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				$userData['username'] = $session_data['username'];
				$this->load->helper('url');
				$this->load->helper('form');
				$this->load->library('form_validation');
				$data['attributes'] = array('class' => 'form-horizontal', 'id' => 'regform', 'style' => 'width:1050px;margin:50px auto;');
				
					$this->form_validation->set_rules('username', 'User Name', 'required');   
					$this->form_validation->set_rules('old_password', 'Old Password', 'required');   
					$this->form_validation->set_rules('new_password', 'New Password', 'required|trim|min_length[6]|matches[new_con_password]');
					$this->form_validation->set_rules('new_con_password', 'Password Confirmation', 'required|trim');
				
			if($this->form_validation->run() === FALSE) {
					$this->load->view('templates/header', $data);	
					$this->load->view('change_password');
					$this->load->view('templates/footer');
				} 
				else {
					$this->password();
					$this->load->view('templates/header', $data);	
					$this->load->view('password_success');
					$this->load->view('templates/footer');
				}
			}
		else{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	public function password(){
   $this->load->model('change_pass');
   $username = $this->input->post('username');
   $old_pass = $this->input->post('old_password');
   $new_pass = $this->input->post('new_password');

   $result = $this->change_pass->changePassword($username, $old_pass, $new_pass);
		
	}

}
?>