<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Registration extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}
	public function index() {
		
		$this->load->model('register_model');
		
		if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
			$userData['username'] = $session_data['username'];
			$this->load->helper('url');
		
		$data['attributes'] = array('class' => 'form-horizontal', 'id' => 'regform', 'style' => 'width:1050px;margin:auto;');
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$data['title'] = ucfirst("Student Registration");
		$data['message'] = "";
		$data['exsOptions'] = $this->register_model->getExs();
		$data['creditOptions'] = $this->register_model->getCredit();
		
		$this->form_validation->set_rules('date', 'Date', 'required');
		$this->form_validation->set_rules('student', 'Student', 'required');
		$this->form_validation->set_rules('gpa', 'Current GPA', 'required');
		$this->form_validation->set_rules('semester', 'Semester Apply to Department', 'required');
		$this->form_validation->set_rules('id', 'ID #', 'required');
		$this->form_validation->set_rules('pin', 'PIN #', 'required');
		$this->form_validation->set_rules('csyear', 'Course Section Year', 'required');
		
		if($this->form_validation->run() === FALSE) {
			$this->load->view('templates/header', $data);	
			$this->load->view('registration/register');
			$this->load->view('templates/footer');
		} else {
			$this->register_model->set_registration();
			$this->load->view('templates/header', $data);	
			$this->load->view('registration/success');
			$this->load->view('templates/footer');
		}
		}
		else{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
		
	}
    function logout(){
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('login', 'refresh');
	}
	
	function info() {
		$this->load->model("info_model");
		
		$myData = $this->info_model->get_info();
				
		$this->load->library("table");
		
		$this->table->set_heading("Year", "Semester", "Class", "Tally");
		
		$this->table->set_caption("Class Tally");
		
		$tmpl = array ('table_open'  => '<table class="infoTable">');

		$this->table->set_template($tmpl);
		
		$data['table'] = $this->table->generate($myData);
		
		$this->load->helper('form');
		$data['attributes'] = array('class' => 'form-horizontal', 'id' => 'regform', 'style' => 'width:1050px;margin:50px auto;');
		$data['title'] = ucfirst("Registration Info");
		$this->load->view('templates/header', $data);
		$this->load->view('info');
		$this->load->view('templates/footer');
	}
}
?>