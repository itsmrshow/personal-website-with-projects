<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class students extends CI_Controller {
	
	function display(){
		$this->load->model('student_model');
		
		$data['attributes'] = array('class' => 'form-horizontal', 'id' => 'regform', 'style' => 'width:1050px;margin:50px auto;');
		
		$this->load->helper('form');
		$this->load->library('form_validation');

			$data['students'] = $this->student_model->search();
			$this->load->view('templates/header', $data);	
			$this->load->view('students');
			$this->load->view('templates/footer');
	}
}
?>
