<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class edit extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}
	
	public function id($s_id = null){
		
		$this->load->model('edit_model');

		if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
			$userData['username'] = $session_data['username'];
			$this->load->helper('url');
		
			$data['attributes'] = array('class' => 'form-horizontal', 'id' => 'regform', 'style' => 'width:1050px;margin:50px auto;');
			
			$this->load->helper('form');
			$this->load->library('form_validation');
			
			$data['student'] = $this->edit_model->getStudent($s_id);
						
			$data['title'] = ucfirst("Edit Student");
			
			$data['studentId'] = $this->edit_model->getStudentID($s_id);
			
			$data['pin'] = $this->edit_model->getPin($s_id);
			
			$advisor = $this->edit_model->getAdvisor($s_id);
			
			$varArray = array("a1","a2","a3","a4","a5","a6","a7","a8","a9","a10","a11","a12","a13","a14","a15","a16","a17","a18");
			
			foreach($varArray as $adv) {
				$data[$adv] = "";
			}
						
			switch($advisor) {
				case "Axtell, Robert":
					$data['a1'] = "selected='selected'";
					break;
				case "Calahan, Susan":
					$data['a2'] = "selected='selected'";
					break;
				case "Davis, Charles":
					$data['a3'] = "selected='selected'";
					break;
				case "Fede, Marybeth":
					$data['a4'] = "selected='selected'";
					break;
				case "Gregory, Robert":
					$data['a5'] = "selected='selected'";
					break;
				case "Hannah, Corey":
					$data['a6'] = "selected='selected'";
					break;
				case "Kemler, David":
					$data['a7'] = "selected='selected'";
					break;
				case "Lamonica, Aukje":
					$data['a8'] = "selected='selected'";
					break;
				case "Latchman, Peter":
					$data['a9'] = "selected='selected'";
					break;
				case "Lunn, William":
					$data['a10'] = "selected='selected'";
					break;
				case "Marino, Doris":
					$data['a11'] = "selected='selected'";
					break;
				case "Misasi, Sharon":
					$data['a12'] = "selected='selected'";
					break;
				case "Morin, Gary":
					$data['a13'] = "selected='selected'";
					break;
				case "Panichas, Pat":
					$data['a14'] = "selected='selected'";
					break;
				case "Rauschenbach, Jim":
					$data['a15'] = "selected='selected'";
					break;
				case "Rothbard, Matt":
					$data['a16'] = "selected='selected'";
					break;
				case "Swartz, Daniel":
					$data['a17'] = "selected='selected'";
					break;
				case "Yang, Jinjin":
					$data['a18'] = "selected='selected'";
					break;
			}
			
			$data['GPA'] = $this->edit_model->getGPA($s_id);
			
			$accept = $this->edit_model->getAccept($s_id);
			
			$data['yes'] = "";
			$data['no'] = "";
			
			if($accept == 1) {
				$data['yes'] = "checked='checked'";
			} else {
				$data['no'] = "checked='checked'";
			}
			
			$semester = $this->edit_model->getSemester($s_id);
			
			$semVals = explode("-", $semester);
						
			$data['applyYear'] = trim($semVals[1]);
			
			$data['applyFall'] = "";
			$data['applySpring'] = "";
			
			if(trim($semVals[0]) == "Fall") {
				$data['applyFall'] = "selected='selected'";
			} else {
				$data['applySpring'] = "selected='selected'";
			}
			
			$exs = $this->edit_model->get_student_exs($s_id);
			$exsOptions = $this->edit_model->getExs();
			
			$oldYear = $exs[0]->year;
			$oldSemester = $exs[0]->semester;
						
			$ex = Array();
			$ex['exs1'] = Array();
			$ex['exs2'] = Array();
			$ex['exs3'] = Array();
			
			$count = 1;
			
			foreach($exs as $e) {
				$newYear = $e->year;
				$newSemester = $e->semester;
				
				if($newYear != $oldYear || $newSemester != $oldSemester) {
					$count++;
					$name = "exs{$count}";
					$ex[$name][] = $e;
					
					$oldYear = $newYear;
					$oldSemester = $newSemester;
				} else {
					$name = "exs{$count}";
					
					$ex[$name][] = $e;
				}
			}
						
			$data['mainYear'] = $ex['exs1'][0]->year;
			$data['mainFall'] = "";
			$data['mainSpring'] = "";
			
			if($ex['exs1'][0]->semester == "Fall") {
				$data['mainFall'] = "selected='selected'";
			} else {
				$data['mainSpring'] = "selected='selected'";
			}

			$data['preYear1'] = "";
			$data['preFall1'] = "";
			$data['preSpring1'] = "";
			
			if(count($ex['exs2']) > 0) {
				$data['preYear1'] = $ex['exs2'][0]->year;
				
				if($ex['exs2'][0]->semester == "Fall") {
					$data['preFall1'] = "selected='selected'";
				} else {
					$data['preSpring1'] = "selected='selected'";
				}
			}
			
			$data['preYear2'] = "";
			$data['preFall2'] = "";
			$data['preSpring2'] = "";
			
			if(count($ex['exs3']) > 0) {
				$data['preYear2'] = $ex['exs3'][0]->year;
				
				if($ex['exs3'][0]->semester == "Fall") {
					$data['preFall2'] = "selected='selected'";
				} else {
					$data['preSpring2'] = "selected='selected'";
				}
			}

			for($xy = 1; $xy <= 8; $xy++) {
				$name1 = "exsOptions{$xy}";
				$name2 = "cexsOptions{$xy}";
				$name3 = "dexsOptions{$xy}";
				
				$data[$name1] = $exsOptions;
				$data[$name2] = $exsOptions;
				$data[$name3] = $exsOptions;
				
				if(array_key_exists($xy - 1, $ex['exs1'])) {
					if(strlen($ex['exs1'][$xy - 1]->exs) < 3) {
						$ex['exs1'][$xy - 1]->exs = "0".$ex['exs1'][$xy - 1]->exs;
					}
					$data[$name1] = str_replace("{".$ex['exs1'][$xy - 1]->exs."}", "selected='selected'", $data[$name1]);
				}
				
				if(array_key_exists($xy - 1, $ex['exs2'])) {
					if(strlen($ex['exs2'][$xy - 1]->exs) < 3) {
						$ex['exs2'][$xy - 1]->exs = "0".$ex['exs2'][$xy - 1]->exs;
					}
					$data[$name2] = str_replace("{".$ex['exs2'][$xy - 1]->exs."}", "selected='selected'", $data[$name2]);
				}
				
				if(array_key_exists($xy - 1, $ex['exs3'])) {
					if(strlen($ex['exs3'][$xy - 1]->exs) < 3) {
						$ex['exs3'][$xy - 1]->exs = "0".$ex['exs3'][$xy - 1]->exs;
					}
					$data[$name3] = str_replace("{".$ex['exs3'][$xy - 1]->exs."}", "selected='selected'", $data[$name3]);
				}
				
				$data[$name1] = preg_replace("/\{\d{3}\}/", "", $data[$name1]);
				$data[$name2] = preg_replace("/\{\d{3}\}/", "", $data[$name2]);
				$data[$name3] = preg_replace("/\{\d{3}\}/", "", $data[$name3]);
			}
			
			$data['creditOptions'] = $this->edit_model->getCredit($s_id);
			
			$this->form_validation->set_rules('date', 'Date', 'required');
			$this->form_validation->set_rules('student', 'Student', 'required');
			$this->form_validation->set_rules('gpa', 'Current GPA', 'required');
			$this->form_validation->set_rules('semester', 'Semester Apply to Department', 'required');
			$this->form_validation->set_rules('id', 'ID #', 'required');
			$this->form_validation->set_rules('pin', 'PIN #', 'required');
			$this->form_validation->set_rules('csyear', 'Course Section Year', 'required');
			
			if($this->form_validation->run() === FALSE) {
				$this->load->view('templates/header', $data);	
				$this->load->view('edit_view');
				$this->load->view('templates/footer');
			} else {
				$this->edit_model->edit_registration();
				$this->load->view('templates/header', $data);	
				$this->load->view('edit_success');
				$this->load->view('templates/footer');
			}
		}
		else{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	 function del($id = null){	
	$this->load->model('edit_model');
	$this->edit_model->delete($id);
   
	 }
}
?>