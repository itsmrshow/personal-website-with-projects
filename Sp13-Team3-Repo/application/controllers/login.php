<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

 function __construct()
 {
   parent::__construct();
 }

 function index()
 {
   $this->load->helper(array('form'));
   $this->load->view('login_view');
 }

 function create_member(){
	 $this->load->library('form_validation');
	 
	 $this->form_validation->set_rules('first_name', 'Name' , 'trim|required');
	 $this->form_validation->set_rules('last_name', 'Last Name' , 'trim|required');
	 $this->form_validation->set_rules('email_address', 'Email Address' , 'trim|required|valid_email');
	 
	 $this->form_validation->set_rules('username', 'Username' , 'trim|required');
	 $this->form_validation->set_rules('password', 'Password' , 'trim|required');
	 $this->form_validation->set_rules('password2', 'Password Confirmation' , 'trim|required|matches[password]');
	 
	 if($this->form_validation->run() == FALSE)
	 {
		$this->load->view('signup_form'); 
	 }
	 else
	 {
		$this->load->model('user');
		if($query = $this->user->create_member())
		{
			$data['main_content'] = 'signup_successful';
			$this->load->view('templates/header', $data);	
			$this->load->view('account_success');
			$this->load->view('templates/footer');
		}
		else{
		$this->load->view('signup_form');	
		}
	 }
 }

}

?>