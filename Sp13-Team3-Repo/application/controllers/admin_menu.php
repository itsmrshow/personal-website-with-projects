<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class admin_menu extends CI_Controller {
	public function __construct() {
		parent::__construct();
	}
	public function index() {
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				$userData['username'] = $session_data['username'];
				$this->load->helper('url');
				$this->load->helper('form');
				$this->load->library('form_validation');
			if($this->form_validation->run() === FALSE) {	
				$this->load->view('frontpage_view_admin');

			}
		   else{
			 redirect('login_view', 'refresh');
			}
		}
		else{
			//If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
    function logout(){
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('login', 'refresh');
	}

}
?>