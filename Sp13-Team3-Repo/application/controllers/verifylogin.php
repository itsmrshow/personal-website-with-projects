<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLogin extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
 }

 function index()
 {
   //This method will have the credentials validation
   $this->load->library('form_validation');
   
   $data['attributes'] = array('class' => 'form-horizontal', 'id' => 'regform', 'style' => 'width:1050px;margin:50px auto;');

   $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
   $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
     $this->load->view('login_view', $data);
   }
   else
   {
     //Go to private area
	 $hold = $this->check_database_admin();
	 if($hold == FALSE){
     redirect('menu', 'refresh', $data);
	 }
	 else{
	redirect('admin_menu', 'refresh', $data); 
	 }
   }

 }

 function check_database($password){
   //Field validation succeeded.  Validate against database
   $username = $this->input->post('username');

   //query the database
   $result = $this->user->login($username, $password);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'username' => $row->username
       );
       $this->session->set_userdata('logged_in', $sess_array);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Invalid username or password');
     return false;
   }
 }
 
  function check_database_admin(){
   //Field validation succeeded.  Validate against database
   $username = $this->input->post('username');

   //query the database
   $result = $this->user->login_admin($username);

   if($result)
   {
     return TRUE;
   }
   else
   {
     return false;
   }
 }
}
?>