<?php
class Info_model extends CI_Model {
	
	public function get_info() {
		$this->db->order_by("year", "desc");
		$this->db->order_by("semester", "desc");
		$this->db->order_by("exs", "asc");
		
		$this->db->select("year, semester, exs, tally");
		
		$query = $this->db->get("class_view");
		
		$returnArray = Array();
		
		foreach($query->result() as $row) {
			$returnArray[] = Array();
			
			foreach($row as $key => $value) {
				$returnArray[count($returnArray) - 1][$key] = $value;
			}
		}
		
		return $returnArray;
	}
}