<?php
Class User extends CI_Model
{
 function login($username, $password)
 {
   $this -> db -> select('username, password');
   $this -> db -> from('users');
   $this -> db -> where('username', $username);
   $this -> db -> where('password', SHA1($password));
   $this -> db -> limit(1);

   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 
  function login_admin($username)
 {
   $this -> db -> select('username, admin');
   $this -> db -> from('users');
   $this -> db -> where('username', $username);
   $this -> db -> where('admin', 'yes');
   $this -> db -> limit(1);

   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 
 function create_member(){
	 $new_member_insert_data = array(
	 	'first_name' => $this->input->post('first_name'),
		'last_name' => $this->input->post('last_name'),
		'email_address' => $this->input->post('email_address'),
		'username' => $this->input->post('username'),
		'password' => SHA1($this->input->post('password')),
		'admin' => $this->input->post('admin')
	 );
	 $insert = $this->db->insert('users', $new_member_insert_data);
	 return $insert;
 }
}
?>