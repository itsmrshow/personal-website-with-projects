<?php
class edit_model extends CI_Model {
	
	private $_exsOptions =<<<END
<option value="0">--</option>
<option value="011" {011}>011</option>
<option value="012" {012}>012</option>
<option value="131" {131}>131</option>
<option value="158" {158}>158</option>
<option value="184" {184}>184</option>
<option value="185" {185}>185</option>
<option value="191" {191}>191</option>
<option value="202" {202}>202</option>
<option value="203" {203}>203</option>
<option value="212" {212}>212</option>
<option value="227" {227}>227</option>
<option value="280" {280}>280</option>
<option value="281" {281}>281</option>
<option value="282" {282}>282</option>
<option value="283" {283}>283</option>
<option value="286" {286}>286</option>
<option value="288" {288}>288</option>
<option value="289" {289}>289</option>
<option value="291" {291}>291</option>
<option value="292" {292}>292</option>
<option value="293" {293}>293</option>
<option value="301" {301}>301</option>
<option value="302" {302}>302</option>
<option value="303" {303}>303</option>
<option value="308" {308}>308</option>
<option value="328" {328}>328</option>
<option value="350" {350}>350</option>
<option value="352" {352}>352</option>
<option value="380" {380}>380</option>
<option value="383" {383}>383</option>
<option value="384" {384}>384</option>
<option value="386" {386}>386</option>
<option value="388" {388}>388</option>
<option value="389" {389}>389</option>
<option value="394" {394}>394</option>
<option value="400" {400}>400</option>
<option value="402" {402}>402</option>
<option value="403" {403}>403</option>
<option value="411" {411}>411</option>
<option value="421" {421}>421</option>
<option value="442" {442}>442</option>
<option value="480" {480}>480</option>
<option value="483" {483}>483</option>
<option value="485" {485}>485</option>
<option value="490" {490}>490</option>
<option value="495" {495}>495</option>
<option value="497" {497}>497</option>
<option value="499" {499}>499</option>
<option value="552" {552}>552</option>
<option value="554" {554}>554</option>
<option value="558" {558}>558</option>
<option value="570" {570}>570</option>
<option value="572" {572}>572</option>
<option value="573" {573}>573</option>
<option value="574" {574}>574</option>
<option value="578" {578}>578</option>
<option value="579" {579}>579</option>
<option value="583" {583}>583</option>
<option value="590" {590}>590</option>
<option value="591" {591}>591</option>
<option value="600" {600}>600</option>
END;

	private $_creditOptions =<<<END
<option value=".5">.5</option>
<option value="1">1</option>
<option value="1.5">1.5</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
END;

	public function edit_registration() {
		
		$date = $this->input->post('date');
		$advisor = $this->input->post('advisor');
		$student = $this->input->post('student');
		$gpa = $this->input->post('gpa');
		$accept = $this->input->post('accept');
		$semester = $this->input->post('season') . " - " . $this->input->post('semester');
		$id = $this->input->post('id');
		$pin = $this->input->post('pin');
		$cssemester = $this->input->post('csseason') . " - " . $this->input->post('csyear');
		$csyear = $this->input->post('csyear');
		$csseason = $this->input->post('csseason');
		$exs = $this->input->post('exs');
		$course = $this->input->post('course');
		$credit = $this->input->post('credit');
		$crn = $this->input->post('crn');
		$days = $this->input->post('days');
		$times = $this->input->post('times');
		$cnexs =$this->input->post('cnexs');
		$cn2014 = $this->input->post('cn2014');
		$cr2014 = $this->input->post('cr2014');
		$cnfexs = $this->input->post('cnfexs');
		$cnf2014 = $this->input->post('cnf2014');
		$crf2014 = $this->input->post('crf2014');
		$studentNotes = $this->input->post('studentNotes');
		$chairNotes = $this->input->post('chairNotes');
		$pre1 = $this->input->post('pre1season') . ' - ' . $this->input->post('pre1year');
		$pre1year = $this->input->post('pre1year');
		$pre1season = $this->input->post('pre1season');
		$pre2 = $this->input->post('pre2season') . ' - ' . $this->input->post('pre2year');
		$pre2year = $this->input->post('pre2year');
		$pre2season = $this->input->post('pre2season');
		
		$studentUpdate = array(
			"reg_date" => date("Y-m-d"),
			"advisor" => $advisor,
			"student" => $student,
			"gpa" => $gpa,
			"accept" => $accept == "yes" ? 1 : 0,
			"semester" => $semester,
			"pin" => $pin
		);
		
		$this->db->where("student_id", $id);
		$this->db->update("student", $studentUpdate);
		
		$this->db->delete("student_exs", array("student_id" => $id));
		
		for($xy = 0; $xy < count($exs); $xy++) {
			if($exs[$xy] != "0" || trim($course[$xy]) != "") {
				$exsInsert = array(
					"student_id" => $id,
					"year" => $csyear,
					"semester" => $csseason, 
					"exs" => $exs[$xy]
				);
				
				$query_string = $this->db->insert_string("student_exs", $exsInsert);
				
				$this->db->query($query_string.' ON DUPLICATE KEY UPDATE student_id = student_id');
			}
		}
		
		for($xy = 0; $xy < count($cnexs); $xy++) {
			if($cnexs[$xy] != "0" || trim($cn2014[$xy]) != "") {
				$exsInsert = array(
					"student_id" => $id,
					"year" => $pre1year,
					"semester" => $pre1season, 
					"exs" => $cnexs[$xy]
				);
				
				$query_string = $this->db->insert_string("student_exs", $exsInsert);
				
				$this->db->query($query_string.' ON DUPLICATE KEY UPDATE student_id = student_id');	
			}	
		}
		
		for($xy = 0; $xy < count($cnfexs); $xy++) {
			if($cnfexs[$xy] != "0" || trim($cnf2014[$xy]) != "") {
				$exsInsert = array(
					"student_id" => $id,
					"year" => $pre2year,
					"semester" => $pre2season,
					"exs" => $cnfexs[$xy]
				);
				
				$query_string = $this->db->insert_string("student_exs", $exsInsert);
				
				$this->db->query($query_string.' ON DUPLICATE KEY UPDATE student_id = student_id');
			}
		}
	}

	public function getExs() {
		return $this->_exsOptions;
	}
	
	public function getStudentId($id){
		$q = $this->db->select('student_id');
		$ret = $this-> db ->get_where('student', array('student_id' => $id));
		$student_id = $ret->result();
		return $student_id[0]->student_id;	
	}

	public function getPin($id) {
		$q = $this->db->select('pin');
		$ret = $this-> db ->get_where('student', array('student_id' => $id));
		$pin = $ret->result();
		return $pin[0]->pin;
	}

	public function getStudent($id){
		$q = $this->db->select('student');
		$ret = $this-> db ->get_where('student', array('student_id' => $id));
		$name = $ret->result();	
		return $name[0]->student;
	}
	
	public function getAdvisor($id){
		$q = $this->db->select('advisor');
		$ret = $this-> db ->get_where('student', array('student_id' => $id));
		$advisor = $ret->result();
		return $advisor[0]->advisor;
	}
	
	public function getGPA($id){
		$q = $this->db->select('gpa');
		$ret = $this-> db ->get_where('student', array('student_id' => $id));
		$gpa = $ret->result();
		return $gpa[0]->gpa;	
	}
	
	public function getAccept($id){
		$q = $this->db->select('accept');
		$ret = $this-> db ->get_where('student', array('student_id' => $id));
		$accept = $ret->result();
		return $accept[0]->accept;
	}
	
	public function getSemester($id){
		$q = $this->db->select('semester');
		$ret = $this-> db ->get_where('student', array('student_id' => $id));
		$semester = $ret->result();
		return $semester[0]->semester;
	}
	
	public function get_student_exs($id) {
		$q = $this->db->select('exs, semester, year');
		$this->db->order_by("year", "asc");
		$this->db->order_by("semester", "asc");
		$ret = $this-> db ->get_where('student_exs', array('student_id' => $id));
		return $ret->result();
	}
	
	public function getCredit() {
		return $this->_creditOptions;
	}
	function delete($id){
  		$this->db->delete('students', array('id' => $id)); 
	}	
}
?>