-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 23, 2013 at 10:37 PM
-- Server version: 5.5.30-log
-- PHP Version: 5.4.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cscthrth_sp13Team3`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `class_view`
--
CREATE TABLE IF NOT EXISTS `class_view` (
`exs` int(11)
,`year` int(11)
,`semester` varchar(20)
,`tally` bigint(21)
);
-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `student_id` int(11) NOT NULL,
  `reg_date` date NOT NULL DEFAULT '0000-00-00',
  `advisor` varchar(255) NOT NULL DEFAULT '',
  `student` varchar(255) NOT NULL DEFAULT '',
  `gpa` double(3,2) NOT NULL DEFAULT '0.00',
  `accept` tinyint(4) NOT NULL DEFAULT '0',
  `semester` varchar(255) NOT NULL DEFAULT '',
  `pin` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_id`, `reg_date`, `advisor`, `student`, `gpa`, `accept`, `semester`, `pin`) VALUES
(12121321, '2013-04-18', 'Axtell, Robert', 'john su', 4.00, 1, 'Spring - 3000', 234232),
(12131313, '2013-04-18', 'Axtell, Robert', 'Mich Rich', 2.00, 1, 'Spring - 2012', 131312),
(22334567, '2013-04-18', 'Axtell, Robert', 'Ismail Jones', 4.00, 1, 'Spring - 2012', 789000),
(23232323, '2013-04-18', 'Axtell, Robert', 'you free', 4.00, 1, 'Spring - 2012', 232423),
(32324234, '2013-04-15', 'Axtell, Robert', 'Mike John', 4.00, 1, 'Spring - 2012', 343422),
(42424244, '2013-04-18', 'Axtell, Robert', 'tom d', 4.00, 1, 'Spring - 2012', 242424),
(44454545, '2013-04-18', 'Axtell, Robert', 'gio gonz', 4.00, 1, 'Spring - 2014', 454546),
(75029348, '2013-04-16', 'Gregory, Robert', 'John Smith', 3.10, 0, 'Spring - 2011', 211892);

-- --------------------------------------------------------

--
-- Table structure for table `student_exs`
--

CREATE TABLE IF NOT EXISTS `student_exs` (
  `student_id` int(11) NOT NULL DEFAULT '0',
  `exs` int(11) NOT NULL DEFAULT '0',
  `year` int(11) NOT NULL DEFAULT '2000',
  `semester` varchar(20) NOT NULL DEFAULT 'Fall',
  PRIMARY KEY (`student_id`,`exs`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_exs`
--

INSERT INTO `student_exs` (`student_id`, `exs`, `year`, `semester`) VALUES
(12121321, 212, 2000, 'Fall'),
(12121321, 301, 2000, 'Fall'),
(12131313, 202, 2000, 'Fall'),
(12131313, 286, 2000, 'Fall'),
(12131313, 288, 2000, 'Fall'),
(22334567, 184, 0, 'Fall'),
(22334567, 289, 0, 'Fall'),
(23232323, 282, 0, 'Fall'),
(23232323, 302, 0, 'Fall'),
(32324234, 191, 2000, 'Fall'),
(32324234, 282, 2000, 'Fall'),
(32324234, 283, 2000, 'Fall'),
(32324234, 288, 2000, 'Fall'),
(32324234, 590, 2000, 'Fall'),
(42424244, 350, 2000, 'Fall'),
(44454545, 291, 2000, 'Fall'),
(75029348, 11, 2000, 'Fall'),
(75029348, 12, 2000, 'Fall'),
(75029348, 185, 2000, 'Fall'),
(75029348, 191, 2000, 'Fall'),
(75029348, 203, 2000, 'Fall'),
(75029348, 227, 2000, 'Fall'),
(75029348, 282, 2000, 'Fall'),
(75029348, 286, 2000, 'Fall');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `first_name` varchar(35) NOT NULL,
  `last_name` varchar(35) NOT NULL,
  `email_address` varchar(55) NOT NULL,
  `username` varchar(35) NOT NULL,
  `password` varchar(40) NOT NULL,
  `admin` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`first_name`, `last_name`, `email_address`, `username`, `password`, `admin`) VALUES
('admin', 'admin', 'admin@southernct.edu', 'admin', '6768d65902b92c482c56af279eb554ccaedebd1a', 'yes');

-- --------------------------------------------------------

--
-- Structure for view `class_view`
--
DROP TABLE IF EXISTS `class_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`cscthrth_Team3`@`localhost` SQL SECURITY DEFINER VIEW `class_view` AS select `student_exs`.`exs` AS `exs`,`student_exs`.`year` AS `year`,`student_exs`.`semester` AS `semester`,count(0) AS `tally` from `student_exs` group by `student_exs`.`exs`,`student_exs`.`year`,`student_exs`.`semester`;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `student_exs`
--
ALTER TABLE `student_exs`
  ADD CONSTRAINT `fk_se_student` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
