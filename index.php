<!DOCTYPE HTML>
<!--
	Prologue 1.2 by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Rob Seccareccia | Software Engineer</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600" rel="stylesheet" type="text/css" />
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="components/jquery/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-panels.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel-noscript.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-wide.css" />
		</noscript>
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
	</head>
	<body>
		<!-- Header -->
			<div id="header" class="skel-panels-fixed">

				<div class="top">

				<?php $grav = md5( strtolower( trim( "rob@crisply.com" ) ) );?>

					<!-- Logo -->
						<div id="logo">
							<span class="image avatar48"><img src="<?php echo 'http://www.gravatar.com/avatar/'. $grav ?>" alt="" /></span>
							<h1 id="title">Rob Seccareccia</h1>
							<span class="byline">Software Engineer</span>
						</div>

					<!-- Nav -->
						<nav id="nav">
							<ul>
								<li><a href="#top" id="top-link" class="skel-panels-ignoreHref"><span class="fa fa-home">Welcome</span></a></li>
								<li><a href="#portfolio" id="portfolio-link" class="skel-panels-ignoreHref"><span class="fa fa-th">Personal Projects</span></a></li>
								<li><a href="#about" id="about-link" class="skel-panels-ignoreHref"><span class="fa fa-user">About Me</span></a></li>
								<li><a href="#contact" id="contact-link" class="skel-panels-ignoreHref"><span class="fa fa-envelope">Contact</span></a></li>
							</ul>
						</nav>	
				</div>
				
				<div class="bottom">

					<!-- Social Icons -->
						
					<ul class="icons">
		                <li><a href="https://twitter.com/RSeccareccia" class="fa fa-twitter solo"><span>Twitter</span></a></li>
		                <li><a href="https://www.facebook.com/rob.seccarecciajr.1" class="fa fa-facebook solo"><span>Facebook</span></a></li>
		                <li><a href="https://plus.google.com/u/2/112417451067198027318/about" class="fa fa-google-plus solo"><span>Google+</span></a></li>
		                <li><a href="https://github.com/seccareccir1" class="fa fa-github solo"><span>GitHub</span></a></li>
		                <li><a href="https://www.linkedin.com/pub/robert-seccareccia/3b/965/507/" class="fa fa-linkedin solo"><span>LinkedIn</span></a></li>
           			 </ul>
				
				</div>
			
			</div>

		<!-- Main -->
			<div id="main">
			
				<!-- Intro -->
					<section id="top" class="one">
						<div class="container">

							<header>
								<h3><strong><b>Welcome</b></strong></h3>
								<h1>Check out my Twitter</h1>
							</header>
							<footer>
								<article>
									<a class="twitter-timeline" href="https://twitter.com/RSeccareccia" 
									data-widget-id="436661266006827009">Tweets by @RSeccareccia</a>
									<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],
									p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id))
									{js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";
									fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
									</script>
								</article>
							</footer>
						</div>
					</section>
					
				<!-- Portfolio -->
					<section id="portfolio" class="two">
						<div class="container">
					
							<header>
								<h2>Personal Projects</h2>
							</header>
							<div style="align: center">
							<div class="row">
								<div class="4u">
									<article class="item">
										<header>
											<h1><a href="http://plnkr.co/edit/10XUXoaP2rmp5v8YvS42?p=preview">Random Window Mask</a></h1>
										</header>
									</article>
								</div>
								<div class="4u">
									<article class="item">
										<header>
											<h1><a href="/php-survey/form.html">Class Survey</a></h1>
										</header>
									</article>
								</div>
								<div class="4u">
									<article class="item">
										<header>
											<h1><a href="http://run.plnkr.co/plunks/vZRy4YhM9P8J16fc2J1s/">Avenger Angular Search</a></h1>
										</header>
									</article>
								</div>
							</div>
					</section>

				<!-- About Me -->
					<section id="about" class="three">
						<div class="container">

							<header>
								<h2>About Me</h2>
							</header>

							<img src="images/me_liz_beach.jpg" alt="" />
							
							<p>I am a Software Developer with experience in: Programming/Languages: Java (Distributed Databases: MySQL, SQL Server, Microsoft Access, 
							Applications, Servlets, RMI, JDBC, Java Applets); Visual Postgresql.
							Basic, PHP, MySQL, HTML, CSS, Ruby on Rails, Perl,
							JavaScript, Python, AngularJS, Node JS, CoffeeScript,
							Participated in weekly staff meetings and Agile team meetings.
							Developed software with sublime text, and Xcode5. Used Bitbucket as source control, and JIRA issue tracking.
							Developed various mobile applications on both Android and IOS.
							Node-Webkit. Developed with Agile programming methodologies.
							Design & IDE Tools: UML, Adobe Creative Suite 6/5/4/3, Notepad++, Sublime Text, Visio, Eclipse, Rubymine, Dr. Java
							Operating Systems: Windows 10/8.1/8/7/Vista/XP/Server 2003/2008/2012/2012 r2, Mac OS X, Multiple Linux Distributions
							Office Tools: MS Word, Excel, PowerPoint, Outlook, Visio
							Certifications: MTA 349, 365, 366, 367</p>

						</div>
					</section>
			
				<!-- Contact -->
					<section id="contact" class="four">
						<div class="container">

							<header>
								<h2>Contact</h2>
							</header>

							<p>Feel free to contact me with you questions.</p>
							
							<form action="MAILTO:support@robseccareccia.com" method="post" enctype="text/plain">
								<div class="row half">
									<div class="6u"><input type="text" class="text" name="name" placeholder="Name" /></div>
									<div class="6u"><input type="text" class="text" name="email" placeholder="Email" /></div>
								</div>
								<div class="row half">
									<div class="12u">
										<textarea name="message" placeholder="Message"></textarea>
									</div>
								</div>
								<div class="row">
									<div class="12u">
										<a href="#" class="button submit">Send Message</a>
									</div>
								</div>
							</form>

						</div>
					</section>
			
			</div>

		<!-- Footer -->
			<div id="footer">
				
				<!-- Copyright -->
					<div class="copyright">
						<p>&copy; 2015 Robert Seccareccia. All rights reserved.</p>
					</div>
					
				<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<!-- ad_one -->
				<ins class="adsbygoogle"
				     style="display:inline-block;width:728px;height:90px"
				     data-ad-client="ca-pub-7360578851963976"
				     data-ad-slot="7877919646"></ins>
				<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			</div>

	</body>
</html>