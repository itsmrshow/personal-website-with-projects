<?php
//Name: Robert Seccareccia
//Course: 443
//Assignment: 4
$t1 = $_GET['t1']; 
$t2 = $_GET['t2']; 
$t3 = $_GET['t3']; 
$t4 = $_GET['t4'];
$t5 = $_GET['t5']; 
$data[0] = $t1;
$data[1] = $t2;
$data[2] = $t3;
$data[3] = $t4;
$data[4] = $t5;

$image = imagecreatetruecolor(200, 200);

$white = imagecolorallocate($image, 255, 255, 255);
#c array for the colors in pie chart for the top pie

$c[0] = imagecolorallocate($image, 255,0,0);
$c[1] = imagecolorallocate($image, 178,34,34);
$c[2] = imagecolorallocate($image, 0,255,0);
$c[3] = imagecolorallocate($image, 0,100,0);
$c[4] = imagecolorallocate($image, 0,0,255);
#darkcolor array that holds color values for the bottom layer

$darkcolor[0] = imagecolorallocate($image, 255,0,0);
$darkcolor[1] = imagecolorallocate($image, 178,34,34);
$darkcolor[2] = imagecolorallocate($image, 0,255,0);
$darkcolor[3] = imagecolorallocate($image, 0,100,0);
$darkcolor[4] = imagecolorallocate($image, 0,0,255);


$datasum = array_sum($data);
$anglesum[0] = 0;
$angle[0] = 0;
$i = 0;
#image fill to assign white to the background color

imagefilledrectangle($image,0,0,200,200,$white);

while ($i <= 4) {
    ++$i;
    $n = $i - 1;
    $part[$i] = $data[$n] / $datasum;
    $angle[$i] = floor($part[$i] * 360);
    $anglesum[$i] = array_sum($angle);
}
#a loop to walk through the data and assign it a background color, and create bottom pie chart

$n = 0;$i=0;
while ($n <= 4) {
    ++$n;
    $f = $n - 1;
    if ($angle[$n] != 0) {
        for ($i = 110; $i > 100; $i--) {
            imagefilledarc($image, 100, $i, 200, 100, $anglesum[$f], $anglesum[$n], $darkcolor[$f], IMG_ARC_PIE);
        }
    }
}

#a loop to assign top color values, also to create the pie chart

$i = 0;
while ($i <= 4) {
    ++$i;
    $n = $i - 1;
    if ($angle[$i] != 0) {
       imagefilledarc($image, 100, 100, 200, 100, $anglesum[$n], $anglesum[$i], $c[$n], IMG_ARC_PIE);
    }   
}
#print the image

header('Content-type: image/png');
imagepng($image);
imagedestroy($image);
?>